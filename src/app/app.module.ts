import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContainerComponent } from './container/container.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { CardHolderComponent } from './card-holder/card-holder.component';
import { GroupComponent } from './group/group.component';
import { CardComponent } from './card/card.component';
import { ModalComponent } from './modal/modal.component';
import { AddDirective } from './add.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContainerComponent,
    AddGroupComponent,
    CardHolderComponent,
    GroupComponent,
    CardComponent,
    ModalComponent,
    AddDirective
  ],
  entryComponents: [GroupComponent, CardComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
