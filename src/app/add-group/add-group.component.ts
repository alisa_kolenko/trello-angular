import { Component, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent {
  @Output() onAdd: EventEmitter<any> = new EventEmitter();
  public groupName: string;
  
  isClicked: boolean = false;

  constructor() {}

  toggle() {
    this.isClicked = !this.isClicked;
  }

  calculateClasses() {
    return {
      'add-group': true,
      'add-group-edit': this.isClicked
    }
  }

  addGroup(groupName: string) {
    if(!groupName){
      groupName = "Group title";
    }
    this.groupName = groupName;
    console.log(groupName);
    this.onAdd.emit(groupName);
    console.log("New group created!");
  }
}
