import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { GroupComponent } from '../group/group.component';
import { AddDirective } from '../add.directive';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent {
  @ViewChild(AddDirective) appAdd: AddDirective;
  public title;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  addNewGroup(title) {
    console.log(title);
    this.title = title;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(GroupComponent);
    const viewContainerRef = this.appAdd.viewContainerRef;
    const componentRef = viewContainerRef.createComponent(componentFactory);
    componentRef.instance.title = this.title;
  }

  removeGroup() {
    const viewContainerRef = this.appAdd.viewContainerRef;
    viewContainerRef.clear();
  }
}
